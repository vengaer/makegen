# makegen

A simple bash script for generating C and C++ Makefiles. 

[![Build Status](https://gitlab.com/vilhelmengstrom/makegen/badges/master/pipeline.svg)](https://gitlab.com/vilhelmengstrom/makegen/commits/master)

Written with gcc and clang in mind but should work for most compilers (may, however, emit warnings since the names are not known).  
Command line switches are modeled after gcc (where applicable), see `makegen -h` for the full list.

## Makefile Properties
The generated Makefile provides a number of features, including but not limited to
- Separate build dir for object files
- Subdirectories of `src` are automatically added as include directories
- Generates Makefile dependencies s.t. changes to headers trigger recompilation of relevant source files
- Limited support for static and dynamic linking in generated Makefile

## Dependencies

- GNU getopt (included in Linux, not in BSD and Mac)
